uint8_t status;
int x_acc;
int y_acc;
int z_acc;

long int x_off = 0;
long int y_off = 0;
long int z_off = 0;

void do_acc_calibration()
{
  int i = 0;
  for (i = 0; i < 1000; i++) {
      x_off += ACC.getX();
      y_off += ACC.getY();
      x_off += ACC.getZ();
      delay(1);
  }
  x_off /= 1000;
  y_off /= 1000;
  z_off /= 1000;
  USB.printf("#cal#x#%ld#y#%ld#z#%ld#\n", x_off, y_off, z_off);
}

void read_acc()
{
  x_acc = ACC.getX() - x_off;
  y_acc = ACC.getY() - y_off;
  z_acc = ACC.getZ() - z_off;
}

void setup()
{
  ACC.ON();
  USB.ON(); // starts using the serial port
  do_acc_calibration();
}

void loop()
{
  status = ACC.check();
  read_acc();
  
  USB.printf("#acc#x#%ld#y#%ld#z#%ld#\n", x_stacked, y_stacked, z_stacked);
  USB.printf("x_: %d, y_: %d, z_: %d\n", x_acc, y_acc, z_acc);  
}



