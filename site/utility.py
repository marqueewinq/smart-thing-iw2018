import uuid
import json
from db import get_db


def get_new_token():
    return str(uuid.uuid4())


def get_data_list():
    db = get_db()
    data_qry = db.execute(
        "SELECT id, author_id, created, data from datapoint ORDER BY created ASC"
    ).fetchall()
    data_list = []
    for idx, item in enumerate(data_qry):
        id, author_id, created, data = item
        try:
            data = json.loads(data)
        except ValueError:
            print(">>>>>WARN can't parse data point id={} data='{}'".format(id, data))
            continue
        data_list.append({"time": str(created), "data": data, "author_id": author_id})
    return data_list


def get_user_list():
    db = get_db()
    user_qry = db.execute("SELECT id, token FROM user").fetchall()
    user_list = []
    for id, token in user_qry:
        user_list.append({"id": id, "token": token})
    return user_list
