build:
	docker build -t smart-thing-docker:latest .

test:
	docker run \
		-p 0.0.0.0:80:5000 \
		-e DATABASE_PATH="/db.sqlite" \
		-v `pwd`:/src/ \
		-it smart-thing-docker /bin/bash \
			-c "pytest /src/."

run: build
	docker run \
		-p 0.0.0.0:80:5000 \
		-e DATABASE_PATH="/db.sqlite" \
		-v `pwd`:/src/ \
		-it smart-thing-docker /bin/bash \
			-c "python /src/site/main.py  && /bin/bash"
