-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS datapoint;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  token TEXT NOT NULL
);

-- data field is JSON
CREATE TABLE datapoint (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER DEFAULT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id)
);